create table department (
    dept_id integer not null,
    dept_name varchar(30) not null,
    dept_location varchar(30) not null,
    unique(dept_id)  
);

create table employee (
    emp_id integer not null,
    emp_name varchar(50) not null,
    dept_id integer not null,
    salary integer not null,
    unique(emp_id)   
);
/*
 * Each record in the table department represents a department which might hire some employees.
 * Each record in the table employee represents an employee who works for one of the departments
 * from the table department. The salary of each employee is known. (However, the locations of 
 * the departments are not relevant here).
 * Write an SQL query that returns a table comprising all the departments (dept_id) in the table
 * department that hire at least one employee, the number of people they employ and the sum of 
 * salaries in each department. the table should be ordered by dept_id (in increasing order). 
 */


select d.dept_id, count(e.emp_id) as count, sum(e.salary) as sum_of_salary 
from department d, employee e
where d.dept_id = e.dept_id
group by d.dept_id
order by d.dept_id;